#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <stdio.h>

#include "../common_funcs/common_funcs.h"

using namespace std;

struct S_inet_info
{
	string ip;
	string mask;
	string broadcast;
};

class I_OS_method
{
public:
	virtual vector<string> get_info_inet() = 0;
	virtual S_inet_info create_struct_info_inet() = 0;
	virtual void start_scripts_inet_info() = 0;
	virtual void start_scripts_pip_install() = 0;
};

class OS : public I_OS_method
{
private:
	void start_scripts_pip_install() override;
	vector<string> get_info_inet() override;
public:
	S_inet_info create_struct_info_inet() override;
};

class Windows : public OS
{
public:
	void start_scripts_inet_info() override;
};


class Linux : public OS
{
public:
	void start_scripts_inet_info() override;
};

class Termux : public OS
{
public:
	void start_scripts_inet_info() override;
};

class Initialization
{
public:
	string get_os();
	S_inet_info start(I_OS_method* os);
};

S_inet_info init_net(OS* os, Initialization& init);
