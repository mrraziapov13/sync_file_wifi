#include "init.h"

int init()
{
	string windows = "W", linux = "L", termux = "T";
	string off_on_file = "off_on.txt";
	string key_file = "key.txt";
	string file_p = "path.txt";

	Initialization init;

	// check status program
	string off_on = init_variable(off_on_file);
	if (off_on == "off")
	{
		cout << "write to file off_on.txt \"on\", than program will work" << endl;
		exit(1);
	}
	
	string os_str = init_os(init);	

	OS *os;
	if (os_str == windows)
	{
		Windows win;
		os = &win;
	}
	else if (os_str == linux)
	{
		Linux lin;
		os = &lin;
	}
	else if (os_str == termux)
	{
		Termux term;
		os = &term;
	}
	else
	{
		cout << "err, system is not detected [file_sync.cpp]" << endl;
		exit(1);
	}
	S_inet_info inet_info = init_net(os, init);
	
	string key = init_variable(key_file);
	
	string p = init_variable(file_p);

	cout << "\n";
	cout << "path: " << p << endl;
	cout << "key: " << key << endl;
	cout << "on_off_file: " << off_on << endl;
	cout << "os: " << os_str << endl;
	cout << "ip: " << inet_info.ip << endl;
	cout << "mask: " << inet_info.mask << endl;
	cout << "broadcast: " << inet_info.broadcast << endl;
	
	return 0;
}

