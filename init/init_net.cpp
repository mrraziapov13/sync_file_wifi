#include "init_net.h"

string windows = "W", linux = "L", termux = "T";

S_inet_info init_net(OS *os, Initialization &init)
{
	os->start_scripts_inet_info();
	S_inet_info inet_info;
	inet_info = init.start(os);
	return inet_info;
}

void OS::start_scripts_pip_install()
{
	string pip_install = "pip install -r requirements.txt";
	system(pip_install.c_str());
	create_file("load_libr_py.txt", "yes");
}

vector<string> OS::get_info_inet()
{
	fstream file;
	string name_out_file = "inet_info.txt";
	string temp_out_1 = "";
	string out = "";
	file.open(name_out_file, ios::in);
	if (file.is_open())
	{
		while (!file.eof())
		{
			file >> temp_out_1;
			out += temp_out_1 + "\n";
		}
		file.close();
	}
	else
	{
		cout << "err open file inet_info.txt [file_sync.h]";
		exit(1);
	}
	vector<string> inet_info = split(out, ";");
	
	return inet_info;
}

S_inet_info OS::create_struct_info_inet()
{
	start_scripts_inet_info();
	if (!check_file("load_libr_py.txt"))
	{
		start_scripts_pip_install();
	}
	S_inet_info inet_info;
	vector<string> ii = get_info_inet();
	inet_info.ip = ii[0];
	inet_info.mask = ii[1];
	inet_info.broadcast = ii[2];
	return inet_info;
}

void Windows::start_scripts_inet_info()
{
	string command = "python inet_info.py";
	system(command.c_str());
}

void Linux::start_scripts_inet_info()
{
	string command = "python3 inet_info.py";
	system(command.c_str());
}

void Termux::start_scripts_inet_info()
{
	string command = "python3 inet_info.py 1";
	system(command.c_str());
}

string Initialization::get_os()
{
	if (system("termux-info --help") == system("dir"))
	{
		return termux;
	}
	else if (system("ifconfig") == system("dir"))
	{
		return linux;
	}
	else if (system("ipconfig.exe") == system("dir"))
	{
		return windows;
	}
	else
	{
		cout << "err, system is not detected [file_sync.cpp]" << endl;
		exit(1);
	}
}

S_inet_info Initialization::start(I_OS_method* os)
{
	return os->create_struct_info_inet();
}