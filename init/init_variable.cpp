#include "init_variable.h"

string init_variable(string name)
{
	if (check_file(name))
	{
		cout << "the " << name << " is always exists" << endl;
		return get_info_in_file(name);
	}
	string str = get_cin(name);
	create_file(name, str);
	return get_info_in_file(name);
}

string get_cin(string name)
{
	cout << "please write " << name << endl;
	string str;
	cin >> str;
	return str;
}
