#include "init_os.h"

string init_os(Initialization &init)
{
	string windows = "W", linux = "L", termux = "T";
	string os = "";
	string name = "os.txt";

	if (check_file(name))
	{
		return get_info_in_file(name);
	}
	if (init.get_os() == windows)
	{
		Windows win;
		create_file(name, windows);
		return windows;
	}
	else if (init.get_os() == linux)
	{
		Linux lin;
		create_file(name, linux);
		return linux;
	}
	else if (init.get_os() == termux)
	{
		Termux term;
		create_file(name, termux);
		return termux;
	}
	else
	{
		cout << "err, system is not detected [file_sync.cpp]" << endl;
		exit(1);
	}
}
