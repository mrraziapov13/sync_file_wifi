#pragma once

#include <string>
#include <vector>
#include <fstream>

#include <filesystem>
//#include <experimental/filesystem>

using namespace std;

string join(vector<string> arr, string del);

vector<string> split(string str, string del);

string get_current_path();

bool check_file(string name);

void create_file(string name, string str);

string get_info_in_file(string name);