#include "common_funcs.h"


string join(vector<string> arr, string del) {

	string str = "";

	for (int i = 0; i < arr.size(); i++) {
		if (i == arr.size() - 1) {
			str += arr[i];
			continue;
		}
		str += arr[i] + del;
	}

	return str;
}


vector<string> split(string str, string del) {

	vector<string> result;
	string buff = "";

	if (del.length() == 0) {

		for (int i = 0; i < str.length(); i++) {
			string s(1, str[i]);
			result.push_back(s);
		}

		return result;
	}
	else {

		for (int i = 0; i < str.length(); i++) {
			int counter = 0;
			string s(1, str[i]);
			buff += s;

			for (int k = 0; k < del.length(); k++) {
				if (str[i + k] == del[k]) {
					counter++;
					if (counter == del.length()) {
						result.push_back(buff.substr(0, buff.length() - 1));
						buff = "";
						i += k;
					}
				}
			}

			if (i == str.length() - 1) {
				result.push_back(buff);
			}
		}

		return result;
	}
}

string get_current_path()
{
	return (filesystem::current_path()).u8string();
}

bool check_file(string name)
{
	if (filesystem::exists(name) && filesystem::file_size(name) != 0)
	{
		return true;
	}
	return false;
}

void create_file(string name, string os)
{
	fstream f;
	f.open(name, ios::out);
	if (f.is_open())
	{
		f << os << endl;
	}
	f.close();
}

string get_info_in_file(string name)
{
	string info_in_file = "";
	string temp_info_in_file = "";
	fstream f;
	f.open(name, ios::in);
	if (f.is_open())
	{
		if (!f.eof())
		{
			f >> temp_info_in_file;
			info_in_file += temp_info_in_file;
		}
	}
	f.close();
	return info_in_file;
}