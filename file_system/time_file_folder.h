#pragma once

#include <windows.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <vector>
#include <sstream>

#include "../common_funcs/common_funcs.h"

using namespace std;

string get_time_file(string path);
string get_time_folder(string path);
int to_mon(string mon);
string get_normal_time(vector<string> vec);

