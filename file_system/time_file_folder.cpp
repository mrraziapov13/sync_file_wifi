#include "time_file_folder.h"

string get_time_file(string path)
{
	HANDLE hFile;
	TCHAR szBuf[MAX_PATH];
	hFile = CreateFile(path.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	FILETIME lpCreationTime, lpLastAccessTime, lpLastWriteTime;
	if (GetFileTime(hFile, &lpCreationTime, &lpLastAccessTime, &lpLastWriteTime))
	{
		// �� ������ ��� ��, ��� ����� ������� ���� �������� � ���� ���������
		SYSTEMTIME stUTC, stLocal;
		FileTimeToSystemTime(&lpLastWriteTime, &stUTC);
		SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

		string mon = to_string(stLocal.wMonth);
		if (mon.length() < 2) mon = "0" + mon;
		string day = to_string(stLocal.wDay);
		if (day.length() < 2) day = "0" + day;
		string hour = to_string(stLocal.wHour);
		if (hour.length() < 2) hour = "0" + hour;
		string min = to_string(stLocal.wMinute);
		if (min.length() < 2) min = "0" + min;
		string sec = to_string(stLocal.wSecond);
		if (sec.length() < 2) sec = "0" + sec;
		string year = to_string(stLocal.wYear);
		if (year.length() < 2) year = "0" + year;
		string time_id = year + mon + day + hour + min + sec;
		return time_id;
	}
	else
	{
		cout << "err in get_time_file" << endl;
	}
	CloseHandle(hFile);
}

string get_time_folder(string path)
{
	struct stat t_stat;
	stat(path.c_str(), &t_stat);
	struct tm* timeinfo = localtime(&t_stat.st_ctime); // or gmtime() depending on what you want
	string time = asctime(timeinfo);
	if ((time == "0") || (time == ""))
	{
		cout << "err get time folder" << endl;
		exit(1);
	}
	vector<string> time_splited = split(time, ":");
	string time_joined = join(time_splited, "");
	vector<string> time_splited_all = split(time_joined, " ");
	string time_joined_all = join(time_splited_all, "");

	string time_id = get_normal_time(time_splited);
	//string h = hash<string>{}(time_id);
	return time_id;
}

//int main()
//{
//	string fil = "";
//	string fol = "";
//	string time_file = get_time_file(fil);
//	cout << fil << endl;
//	cout << time_file << endl;
//	cout << hash<string>{}(time_file) << endl;
//	cout << "\n";
//	string time_folder = get_time_folder(fol);
//	cout << fol << endl;
//	cout << time_folder << endl;
//	cout << hash<string>{}(time_folder) << endl;
//	return 0;
//}

int to_mon(string mon)
{
	if (mon == "Jan") return 1;
	if (mon == "Feb") return 2;
	if (mon == "Mar") return 3;
	if (mon == "Apr") return 4;
	if (mon == "May") return 5;
	if (mon == "Jun") return 6;
	if (mon == "Jul") return 7;
	if (mon == "Aug") return 8;
	if (mon == "Sep") return 9;
	if (mon == "Oct") return 10;
	if (mon == "Nov") return 11;
	if (mon == "Dec") return 12;
	else
	{
		return 0;
	}
}

string get_normal_time(vector<string> vec)
{
	int temp_mon = to_mon(split(vec.at(0), " ").at(1));
	if (temp_mon == 0)
	{
		cout << "err in create string mon to int mon" << endl;
		exit(1);
	}
	string mon = to_string(temp_mon);
	if (mon.length() < 2) mon = "0" + mon;
	string day = split(vec.at(0), " ").at(3);
	if (day.length() < 2) day = "0" + day;
	string hour = split(vec.at(0), " ").at(4);
	if (hour.length() < 2) hour = "0" + hour;
	string min = split(vec.at(1), " ").at(0);
	if (min.length() < 2) min = "0" + min;
	string sec = split(vec.at(2), " ").at(0);
	if (sec.length() < 2) sec = "0" + sec;
	int temp_year = stoi(split(vec.at(2), " ").at(1));
	string year = to_string(temp_year);
	if (year.length() < 2) year = "0" + year;

	string time_id = year + mon + day + hour + min + sec;
	//int time = stoi(time_id.substr(4, time_id.length()));
	//cout << "warning: int is small for time_id, see later (use long int)" << endl;
	return time_id;
}